import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Contact{
    private String name, phone, email, dob, address, gender;
    public Contact(String name, String phone, String email, String dob, String gender, String address) {
        this.name = name;
        this.phone = phone;
        this.email = email;
        this.dob = dob;
        this.gender = gender;
        this.address = address;
    }

    // Getters

    public String getName() { return this.name; }
    public String getPhone() { return this.phone; }
    public String getEmail() { return this.email; }
    public String getDOB() { return this.dob; }
    public String getGender() { return this.gender; }
    public String getAddress() { return this.address; }

    // Setters

    public void setName(String name) { this.name = name; }
    public void setPhone(String phone) { this.phone = phone; }
    public void setEmail(String email) { this.email = email; }
    public void setDOB(String dob) { this.dob = dob; }
    public void setGender(String gender) { this.gender = gender; }
    public void setAddress(String address) { this.address = address; }

    public static boolean checkNameInput(String nameData) {
        String nameRegex = "([A-Za-z\\s]+)";
        Pattern namePattern = Pattern.compile(nameRegex);
        Matcher nameMatcher = namePattern.matcher(nameData);
        return nameMatcher.matches() ? true : false;
    }
    public static boolean checkPhoneInput(String nameData) {
        String nameRegex = "(\\d{10})";
        Pattern namePattern = Pattern.compile(nameRegex);
        Matcher nameMatcher = namePattern.matcher(nameData);
        return nameMatcher.matches() ? true : false;
    }
    public static boolean checkEmailInput(String nameData) {
        String nameRegex = "([A-Za-z_]([\\.A-Za-z0-9\\+-_]+)*@([A-Za-z_]([A-Za-z0-9-])*)(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z0-9]{2,}))";
        Pattern namePattern = Pattern.compile(nameRegex);
        Matcher nameMatcher = namePattern.matcher(nameData);
        return nameMatcher.matches() ? true : false;
    }
    public static boolean checkDOBInput(String nameData) {
        String nameRegex = "(\\d{2}-\\d{2}-\\d{4})";
        Pattern namePattern = Pattern.compile(nameRegex);
        Matcher nameMatcher = namePattern.matcher(nameData);
        return nameMatcher.matches() ? true : false;
    }
    public static boolean checkGenderInput(String nameData) {
        String nameRegex = "([mMfF])";
        Pattern namePattern = Pattern.compile(nameRegex);
        Matcher nameMatcher = namePattern.matcher(nameData);
        return nameMatcher.matches() ? true : false;
    }
    public static boolean checkAddressInput(String nameData) {
        String nameRegex = "([\\s\\S]+)";
        Pattern namePattern = Pattern.compile(nameRegex);
        Matcher nameMatcher = namePattern.matcher(nameData);
        return nameMatcher.matches() ? true : false;
    }

    @Override
    public String toString() {
        return "\nName: " + this.name + " Phone Number: " + this.phone + " Email: " + this.email + " DOB: " + this.dob + " Gender: " + this.gender + " Address: " + this.address;
    }
}