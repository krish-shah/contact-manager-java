import java.util.*;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableModel;

import java.awt.*;
import java.awt.event.*;

public class ContactDemo extends JFrame{
    JTextField nameField, phoneField, emailField, DOBField, genderField, addressField, searchField;
    JLabel nameLabel, phoneLabel, emailLabel, DOBLabel, genderLabel, addressLabel;
    JButton backButton, addNewSubmitButton, editSubmitButton, cancelButton, addNewContactButton, editButton, deleteButton, searchButton;
    JPanel indexBasePanel, indexHeaderPanel, indexBodyPanel, indexFooterPanel;
    JTable contactsTable;
    JLabel searchLabel;
    ArrayList<Contact> allContacts;
    CardLayout cl;
    boolean isValidInput, isInputUnique, res;
    ArrayList<String> uniqueEmails, uniquePhoneNumbers;
    int selectedItem;
    String filePath = "D:\\Engineering\\Advance Java\\Contact Manager ArrayList\\Contacts.xml";
    public ContactDemo() {
        super("Contacts: ");
        setLayout(new BorderLayout(10, 10));
        setBounds(400, 400, 800, 600);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Container contentPane = getContentPane();
        cl = new CardLayout();
        JPanel baseCard = new JPanel();
        indexBasePanel = new JPanel();
        baseCard.setLayout(cl);

        /* Home Page Start */

        // Index Header
        addNewContactButton = new JButton("add new contact");
        indexHeaderPanel = new JPanel(new BorderLayout());
        indexHeaderPanel.add(addNewContactButton, BorderLayout.EAST);     

        // Index Body 
        allContacts = new ArrayList<>();
        allContacts = XMLParser.getContacts(filePath);
        // allContacts = XMLParser.getContacts("D:\\Engineering\\Advance Java\\ContactManager(UsingArrayList)\\ContactManager(UsingArrayList)\\DBDemo.xml");

        uniqueEmails = new ArrayList<>();
        uniquePhoneNumbers = new ArrayList<>();
        for(Contact contact: allContacts) {
            uniqueEmails.add(contact.getEmail());
            uniquePhoneNumbers.add(contact.getPhone());
        }
        Iterator contactsIterator = allContacts.iterator(); 

        Vector<String> colHeads = new Vector<>();
        colHeads.add("Name");
        colHeads.add("Phone Number");
        colHeads.add("Email");
        colHeads.add("DOB");
        colHeads.add("Gender");
        colHeads.add("address");

        Vector<Vector<String>> colItems = new Vector<>();
        while(contactsIterator.hasNext()) {
            Contact contact = (Contact)contactsIterator.next();
            Vector<String> tuple = new Vector<>();
            tuple.add(contact.getName());
            tuple.add(contact.getPhone());
            tuple.add(contact.getEmail());
            tuple.add(contact.getDOB());
            tuple.add(contact.getGender());
            tuple.add(contact.getAddress());
            colItems.add(tuple);
        }

        DefaultTableModel defTableModel = new DefaultTableModel(colItems, colHeads) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        contactsTable = new JTable(defTableModel);
        contactsTable.setSelectionBackground(Color.CYAN);
        contactsTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        contactsTable.getTableHeader().setReorderingAllowed(false);
        int v = ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED;
        int h = ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED;
        JScrollPane jsp = new JScrollPane(contactsTable, v, h);

        indexBodyPanel = new JPanel();
        indexBodyPanel.add(jsp);

        editButton = new JButton("Edit Contact");
        deleteButton = new JButton("Delete Contact");
        searchLabel = new JLabel("Search: ");
        searchField = new JTextField();
        searchButton = new JButton("Search");

        indexFooterPanel = new JPanel(new GridLayout(1, 5, 5, 10));

        indexFooterPanel.add(editButton);
        indexFooterPanel.add(deleteButton);
        indexFooterPanel.add(searchLabel);
        indexFooterPanel.add(searchField);
        indexFooterPanel.add(searchButton);

        indexBasePanel.add(indexHeaderPanel);
        indexBasePanel.add(indexBodyPanel);
        indexBasePanel.add(indexFooterPanel);

        baseCard.add(indexBasePanel, "Home");
        
        /* Home Page Done */
        
        /* add newContact Page Start */
        
        JPanel addNewContactPageBasePanel = new JPanel(new GridLayout(5,1));

        // Header 
        JPanel addNewContactPageHeader = new JPanel(new FlowLayout());
        JLabel addHeader = new JLabel("Add new contact");
        addNewContactPageHeader.add(addHeader);
        addNewContactPageBasePanel.add(addNewContactPageHeader);

        // Body
        JPanel addNewContactPageBody = new JPanel(new GridLayout(3, 1, 20, 10));
        JPanel addFormFieldsRow1 = new JPanel(new GridLayout(1, 6));
        JPanel addFormFieldsRow2 = new JPanel(new GridLayout(1, 6));
        JPanel addFormFieldsRow3 = new JPanel(new GridLayout(1, 6));

        nameField = new JTextField(25);
        phoneField = new JTextField(25);
        emailField = new JTextField(25);
        DOBField = new JTextField(25);
        genderField = new JTextField(25);
        addressField = new JTextField(25);

        nameLabel = new JLabel("Enter Name: ");
        phoneLabel = new JLabel("Enter Phone Number: ");
        emailLabel = new JLabel("Enter Email: ");
        DOBLabel = new JLabel("Enter DOB: ");
        genderLabel = new JLabel("Enter Gender: ");
        addressLabel = new JLabel("Enter address: ");
        
        JLabel addNewNameStatusLabel = new JLabel("");
        JLabel addNewPhoneStatusLabel = new JLabel("");
        JLabel addNewEmailStatusLabel = new JLabel("");
        JLabel addNewDOBStatusLabel = new JLabel("");
        JLabel addNewGenderStatusLabel = new JLabel("");
        JLabel addNewAddressStatusLabel = new JLabel("");
        
        addFormFieldsRow1.add(nameLabel);
        addFormFieldsRow1.add(nameField);
        addFormFieldsRow1.add(addNewNameStatusLabel);        
        
        addFormFieldsRow1.add(phoneLabel);
        addFormFieldsRow1.add(phoneField);
        addFormFieldsRow1.add(addNewPhoneStatusLabel);
        
        addFormFieldsRow2.add(emailLabel);
        addFormFieldsRow2.add(emailField);
        addFormFieldsRow2.add(addNewEmailStatusLabel);
        
        addFormFieldsRow2.add(DOBLabel);
        addFormFieldsRow2.add(DOBField);
        addFormFieldsRow2.add(addNewDOBStatusLabel);
        
        addFormFieldsRow3.add(genderLabel);
        addFormFieldsRow3.add(genderField);
        addFormFieldsRow3.add(addNewGenderStatusLabel);
        
        addFormFieldsRow3.add(addressLabel);
        addFormFieldsRow3.add(addressField);
        addFormFieldsRow3.add(addNewAddressStatusLabel);
        
        addNewContactPageBody.add(addFormFieldsRow1);
        addNewContactPageBody.add(addFormFieldsRow2);
        addNewContactPageBody.add(addFormFieldsRow3);
        addNewContactPageBasePanel.add(addNewContactPageBody);
        // addNewContactPageBasePanel.add(formFields);
        // Footer
        JPanel addNewContactPageFooter = new JPanel();
        backButton = new JButton("Back");
        addNewSubmitButton = new JButton("Submit");
        addNewContactPageFooter.add(backButton);
        addNewContactPageFooter.add(addNewSubmitButton);
        addNewContactPageBasePanel.add(addNewContactPageFooter);

        baseCard.add(addNewContactPageBasePanel, "New");
        /* add newContact Page End */

        /* ************************************************************ */
        /* ************************************************************ */
        /* ************************************************************ */
        /* ************************************************************ */
        /* ************************************************************ */

        /* Edit Page Start */
        
        JPanel editContactPageBasePanel = new JPanel(new GridLayout(5,1));

        // Header 
        JPanel editContactPageHeader = new JPanel(new FlowLayout());
        JLabel editHeader = new JLabel("Edit a contact");
        editContactPageHeader.add(editHeader);
        editContactPageBasePanel.add(editContactPageHeader);

        // Body
        JPanel editContactPageBody = new JPanel(new GridLayout(3, 1, 20, 10));
        JPanel editFieldsRow1 = new JPanel(new GridLayout(1, 6));
        JPanel editFieldsRow2 = new JPanel(new GridLayout(1, 6));
        JPanel editFieldsRow3 = new JPanel(new GridLayout(1, 6));

        JTextField editNameField = new JTextField(25);
        JTextField editPhoneField = new JTextField(25);
        JTextField editEmailField = new JTextField(25);
        JTextField editDOBField = new JTextField(25);
        JTextField editGenderField = new JTextField(25);
        JTextField editAddressField = new JTextField(25);

        JLabel editNameLabel = new JLabel("Enter Name: ");
        JLabel editPhoneLabel = new JLabel("Enter Phone Number: ");
        JLabel editEmailLabel = new JLabel("Enter Email: ");
        JLabel editDOBLabel = new JLabel("Enter DOB: ");
        JLabel editGenderLabel = new JLabel("Enter Gender: ");
        JLabel editAddressLabel = new JLabel("Enter address: ");
        
        JLabel editContactNameStatusLabel = new JLabel("");
        JLabel editContactPhoneStatusLabel = new JLabel("");
        JLabel editContactEmailStatusLabel = new JLabel("");
        JLabel editContactDOBStatusLabel = new JLabel("");
        JLabel editContactGenderStatusLabel = new JLabel("");
        JLabel editContactAddressStatusLabel = new JLabel("");
        
        editFieldsRow1.add(editNameLabel);
        editFieldsRow1.add(editNameField);
        editFieldsRow1.add(editContactNameStatusLabel);        
        
        editFieldsRow1.add(editPhoneLabel);
        editFieldsRow1.add(editPhoneField);
        editFieldsRow1.add(editContactPhoneStatusLabel);
        
        editFieldsRow2.add(editEmailLabel);
        editFieldsRow2.add(editEmailField);
        editFieldsRow2.add(editContactEmailStatusLabel);    
        
        editFieldsRow2.add(editDOBLabel);
        editFieldsRow2.add(editDOBField);
        editFieldsRow2.add(editContactDOBStatusLabel);
        
        editFieldsRow3.add(editGenderLabel);
        editFieldsRow3.add(editGenderField);
        editFieldsRow3.add(editContactGenderStatusLabel);
        
        editFieldsRow3.add(editAddressLabel);
        editFieldsRow3.add(editAddressField);
        editFieldsRow3.add(editContactAddressStatusLabel);
        
        editContactPageBody.add(editFieldsRow1);
        editContactPageBody.add(editFieldsRow2);
        editContactPageBody.add(editFieldsRow3);
        editContactPageBasePanel.add(editContactPageBody);
        // editContactPageBasePanel.add(formFields);
        // Footer
        JPanel editContactPageFooter = new JPanel(new FlowLayout(FlowLayout.CENTER));
        JButton backButton2 = new JButton("Back");
        editSubmitButton = new JButton("Submit");
        editContactPageFooter.add(backButton2);
        editContactPageFooter.add(editSubmitButton);
        editContactPageBasePanel.add(editContactPageFooter);

        baseCard.add(editContactPageBasePanel, "Edit");
        contentPane.add(baseCard);
        /* Edit Page End */
        
        /* Action Listeners */
        addNewContactButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                cl.show(baseCard, "New");

                nameField.setText("");
                phoneField.setText("");
                emailField.setText("");
                DOBField.setText("");
                genderField.setText("");
                addressField.setText("");

                // nameField.setBorder(new JTextField().getBorder());
                // phoneField.setBorder(new JTextField().getBorder());
                // emailField.setBorder(new JTextField().getBorder());
                // DOBField.setBorder(new JTextField().getBorder());
                // genderField.setBorder(new JTextField().getBorder());
                // addressField.setBorder(new JTextField().getBorder());

                addNewNameStatusLabel.setText("");
                addNewPhoneStatusLabel.setText("");
                addNewEmailStatusLabel.setText("");
                addNewDOBStatusLabel.setText("");
                addNewGenderStatusLabel.setText("");
                addNewAddressStatusLabel.setText("");  
                
                nameField.requestFocus();
            }
        });
        /* --------------------------------------------------- */
        /* ------------------------Add New Start----------------- */
        /* --------------------------------------------------- */

        Border success = BorderFactory.createLineBorder(Color.GREEN);
        Border failure = BorderFactory.createLineBorder(Color.RED);
            
        Color valid = Color.GREEN;
        Color invalid = Color.RED;

        nameField.addFocusListener(new FocusAdapter() {
            public void focusGained(FocusEvent e) {
                Border textFieldBorder = BorderFactory.createLineBorder(Color.BLUE);
                nameField.setBorder(textFieldBorder);
                addNewNameStatusLabel.setText("");
            }
            //  checking regex once typing is done
            public void focusLost(FocusEvent e) {
                String nameContent = nameField.getText();
                res = Contact.checkNameInput(nameContent);
                if(res) {
                    nameField.setBorder(success);
                    addNewNameStatusLabel.setText("Valid");
                    addNewNameStatusLabel.setForeground(valid);
                    isValidInput = true;
                } else {
                    nameField.setBorder(failure);
                    addNewNameStatusLabel.setText("Invalid input");
                    addNewNameStatusLabel.setForeground(invalid);;
                    isValidInput = false;
                }
            }
        });
        
        phoneField.addFocusListener(new FocusAdapter() {
            public void focusGained(FocusEvent e) {
                Border textFieldBorder = BorderFactory.createLineBorder(Color.BLUE);
                phoneField.setBorder(textFieldBorder);
                addNewPhoneStatusLabel.setText("");
            }
            
            public void focusLost(FocusEvent e) {
                String phoneContent = phoneField.getText();
                res = Contact.checkPhoneInput(phoneContent);
                if(res) {
                    phoneField.setBorder(success);
                    addNewPhoneStatusLabel.setText("Valid");
                    addNewPhoneStatusLabel.setForeground(valid);;
                    isValidInput = true;
                } else {
                    phoneField.setBorder(failure);
                    addNewPhoneStatusLabel.setText("Invalid input");
                    addNewPhoneStatusLabel.setForeground(invalid);;
                    isValidInput = false;
                }
            }        
        });
        
        emailField.addFocusListener(new FocusAdapter() {
            public void focusGained(FocusEvent e) {
                Border textFieldBorder = BorderFactory.createLineBorder(Color.BLUE);
                emailField.setBorder(textFieldBorder);
                addNewEmailStatusLabel.setText("");
            }
            
            public void focusLost(FocusEvent e) {
                String emailContent = emailField.getText();
                res = Contact.checkEmailInput(emailContent);
                if(res) {
                    emailField.setBorder(success);
                    addNewEmailStatusLabel.setText("Valid");
                    addNewEmailStatusLabel.setForeground(valid);;
                    isValidInput = true;
                } else {
                    emailField.setBorder(failure);
                    addNewEmailStatusLabel.setText("Invalid input");
                    addNewEmailStatusLabel.setForeground(invalid);;
                    isValidInput = false;
                }
            }        
        });

        DOBField.addFocusListener(new FocusAdapter() {
            public void focusGained(FocusEvent e) {
                Border textFieldBorder = BorderFactory.createLineBorder(Color.BLUE);
                DOBField.setBorder(textFieldBorder);
                addNewDOBStatusLabel.setText("");
            }
            
            public void focusLost(FocusEvent e) {
                String DOBContent = DOBField.getText();
                res = Contact.checkDOBInput(DOBContent);
                if(res) {
                    DOBField.setBorder(success);
                    addNewDOBStatusLabel.setText("Valid");
                    addNewDOBStatusLabel.setForeground(valid);;
                    isValidInput = true;
                } else {
                    DOBField.setBorder(failure);
                    addNewDOBStatusLabel.setText("Invalid input");
                    addNewDOBStatusLabel.setForeground(invalid);;
                    isValidInput = false;
                }
            }        
        });

        genderField.addFocusListener(new FocusAdapter() {
            public void focusGained(FocusEvent e) {
                Border textFieldBorder = BorderFactory.createLineBorder(Color.BLUE);
                genderField.setBorder(textFieldBorder);
                addNewGenderStatusLabel.setText("");
            }
            
            public void focusLost(FocusEvent e) {
                String genderContent = genderField.getText();
                res = Contact.checkGenderInput(genderContent);
                if(res) {
                    genderField.setBorder(success);
                    addNewGenderStatusLabel.setText("Valid");
                    addNewGenderStatusLabel.setForeground(valid);;
                    isValidInput = true;
                } else {
                    genderField.setBorder(failure);
                    addNewGenderStatusLabel.setText("Invalid input");
                    addNewGenderStatusLabel.setForeground(invalid);;
                    isValidInput = false;
                }
            }        
        });

        addressField.addFocusListener(new FocusAdapter() {
            public void focusGained(FocusEvent e) {
                Border textFieldBorder = BorderFactory.createLineBorder(Color.BLUE);
                addressField.setBorder(textFieldBorder);
                addNewAddressStatusLabel.setText("");
            }
            
            public void focusLost(FocusEvent e) {
                String addressContent = addressField.getText();
                res = Contact.checkAddressInput(addressContent);
                if(res) {
                    addressField.setBorder(success);
                    addNewAddressStatusLabel.setText("Valid");
                    addNewAddressStatusLabel.setForeground(valid);;
                    isValidInput = true;
                } else {
                    addressField.setBorder(failure);
                    addNewAddressStatusLabel.setText("Invalid input");
                    addNewAddressStatusLabel.setForeground(invalid);;
                    isValidInput = false;
                }
            }        
        });

        addNewSubmitButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if(isValidInput) {
                    if(uniqueEmails.contains(emailField.getText())) {
                        // System.out.println("Called in unique mail");
                        isInputUnique = false;
                        addNewEmailStatusLabel.setText("This email already exists!");
                        addNewEmailStatusLabel.setForeground(invalid);
                        return;
                    } else if (uniquePhoneNumbers.contains(phoneField.getText())) {
                        // System.out.println("Called in unique phone");
                        isInputUnique = false;
                        addNewPhoneStatusLabel.setText("This phone number already exists");
                        addNewPhoneStatusLabel.setForeground(invalid);
                        return;
                    } else {
                        isInputUnique = true;
                    }
                }

                if(isInputUnique) {
                    // System.out.println("Called");
                    Contact newContact = new Contact(nameField.getText(), phoneField.getText(), emailField.getText(), DOBField.getText(), genderField.getText(), addressField.getText());
                    XMLParser.addContact(newContact, filePath);
                    allContacts.add(newContact);
                    cl.show(baseCard, "Home");
                    XMLParser.getContacts(filePath);
                }
            }
        });
    

        /* --------------------------------------------------- */
        /* ------------------------Edit Start----------------- */
        /* --------------------------------------------------- */


        editAddressField.addFocusListener(new FocusAdapter() {
            public void focusGained(FocusEvent e) {
                Border textFieldBorder = BorderFactory.createLineBorder(Color.BLUE);
                editAddressField.setBorder(textFieldBorder);
                editContactAddressStatusLabel.setText("");
            }
            
            public void focusLost(FocusEvent e) {
                String addressContent = editAddressField.getText();
                res = Contact.checkAddressInput(addressContent);
                if(res) {
                    editAddressField.setBorder(success);
                    editContactAddressStatusLabel.setText("Valid");
                    editContactAddressStatusLabel.setForeground(valid);;
                    isValidInput = true;
                } else {
                    editAddressField.setBorder(failure);
                    editContactAddressStatusLabel.setText("Invalid input");
                    editContactAddressStatusLabel.setForeground(invalid);;
                    isValidInput = false;
                }
            }        
        });

        editGenderField.addFocusListener(new FocusAdapter() {
            public void focusGained(FocusEvent e) {
                Border textFieldBorder = BorderFactory.createLineBorder(Color.BLUE);
                editGenderField.setBorder(textFieldBorder);
                editContactGenderStatusLabel.setText("");
            }
            
            public void focusLost(FocusEvent e) {
                String genderContent = editGenderField.getText();
                res = Contact.checkGenderInput(genderContent);
                if(res) {
                    editGenderField.setBorder(success);
                    editContactGenderStatusLabel.setText("Valid");
                    editContactGenderStatusLabel.setForeground(valid);;
                    isValidInput = true;
                } else {
                    editGenderField.setBorder(failure);
                    editContactGenderStatusLabel.setText("Invalid input");
                    editContactGenderStatusLabel.setForeground(invalid);;
                    isValidInput = false;
                }
            }        
        });

        editDOBField.addFocusListener(new FocusAdapter() {
            public void focusGained(FocusEvent e) {
                Border textFieldBorder = BorderFactory.createLineBorder(Color.BLUE);
                editDOBField.setBorder(textFieldBorder);
                editContactDOBStatusLabel.setText("");
            }
            
            public void focusLost(FocusEvent e) {
                String DOBContent = editDOBField.getText();
                res = Contact.checkDOBInput(DOBContent);
                if(res) {
                    editDOBField.setBorder(success);
                    editContactDOBStatusLabel.setText("Valid");
                    editContactDOBStatusLabel.setForeground(valid);;
                    isValidInput = true;
                } else {
                    editDOBField.setBorder(failure);
                    editContactDOBStatusLabel.setText("Invalid input");
                    editContactDOBStatusLabel.setForeground(invalid);;
                    isValidInput = false;
                }
            }        
        });

        editEmailField.addFocusListener(new FocusAdapter() {
            public void focusGained(FocusEvent e) {
                Border textFieldBorder = BorderFactory.createLineBorder(Color.BLUE);
                editEmailField.setBorder(textFieldBorder);
                editContactEmailStatusLabel.setText("");
            }

            public void focusLost(FocusEvent e) {
                String emailContent = editEmailField.getText();
                res = Contact.checkEmailInput(emailContent);
                if(res) {
                    editEmailField.setBorder(success);
                    editContactEmailStatusLabel.setText("Valid");
                    editContactEmailStatusLabel.setForeground(valid);;
                    isValidInput = true;
                } else {
                    editEmailField.setBorder(failure);
                    editContactEmailStatusLabel.setText("Invalid input");
                    editContactEmailStatusLabel.setForeground(invalid);;
                    isValidInput = false;
                }
            }        
        });
        
        editPhoneField.addFocusListener(new FocusAdapter() {
            public void focusGained(FocusEvent e) {
                Border textFieldBorder = BorderFactory.createLineBorder(Color.BLUE);
                editPhoneField.setBorder(textFieldBorder);
                editContactPhoneStatusLabel.setText("");
            }

            public void focusLost(FocusEvent e) {
                String phoneContent = editPhoneField.getText();
                res = Contact.checkPhoneInput(phoneContent);
                if(res) {
                    editPhoneField.setBorder(success);
                    editContactPhoneStatusLabel.setText("Valid");
                    editContactPhoneStatusLabel.setForeground(valid);;
                    isValidInput = true;
                } else {
                    editPhoneField.setBorder(failure);
                    editContactPhoneStatusLabel.setText("Invalid input");
                    editContactPhoneStatusLabel.setForeground(invalid);;
                    isValidInput = false;
                }
            }        
        });

        editNameField.addFocusListener(new FocusAdapter() {
            public void focusGained(FocusEvent e) {
                Border textFieldBorder = BorderFactory.createLineBorder(Color.BLUE);
                editNameField.setBorder(textFieldBorder);
                editContactNameStatusLabel.setText("");
            }
            //  checking regex once typing is done
            public void focusLost(FocusEvent e) {
                String nameContent = editNameField.getText();
                res = Contact.checkNameInput(nameContent);
                if(res) {
                    editNameField.setBorder(success);
                    editContactNameStatusLabel.setText("Valid");
                    editContactNameStatusLabel.setForeground(valid);;
                    isValidInput = true;
                } else {
                    editNameField.setBorder(failure);
                    editContactNameStatusLabel.setText("Invalid input");
                    editContactNameStatusLabel.setForeground(invalid);;
                    isValidInput = false;
                }
            }
        });

        editSubmitButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                selectedItem = contactsTable.getSelectedRow();
                if(!isDirty(selectedItem, editNameField.getText(), editPhoneField.getText(), editEmailField.getText(), editDOBField.getText(), editGenderField.getText(), editAddressField.getText())) {
                    editContactNameStatusLabel.setText("Nothing was changed");
                    editContactNameStatusLabel.setForeground(invalid);
                    editContactPhoneStatusLabel.setText("Nothing was changed");
                    editContactPhoneStatusLabel.setForeground(invalid);
                    editContactEmailStatusLabel.setText("Nothing was changed");
                    editContactEmailStatusLabel.setForeground(invalid);
                    editContactDOBStatusLabel.setText("Nothing was changed");
                    editContactDOBStatusLabel.setForeground(invalid);
                    editContactGenderStatusLabel.setText("Nothing was changed");
                    editContactGenderStatusLabel.setForeground(invalid);
                    editContactAddressStatusLabel.setText("Nothing was changed");
                    editContactAddressStatusLabel.setForeground(invalid);
                    return;
                }
                
                if(!editNameField.getText().equalsIgnoreCase(allContacts.get(selectedItem).getName()))
                    allContacts.get(selectedItem).setName(editNameField.getText());

                if(!editPhoneField.getText().equalsIgnoreCase(allContacts.get(selectedItem).getPhone()))
                    allContacts.get(selectedItem).setPhone(editPhoneField.getText());

                if(!editEmailField.getText().equalsIgnoreCase(allContacts.get(selectedItem).getEmail()))
                    allContacts.get(selectedItem).setEmail(editEmailField.getText());

                if(!editDOBField.getText().equalsIgnoreCase(allContacts.get(selectedItem).getDOB()))
                    allContacts.get(selectedItem).setDOB(editDOBField.getText());

                if(!editGenderField.getText().equalsIgnoreCase(allContacts.get(selectedItem).getGender()))
                    allContacts.get(selectedItem).setGender(editGenderField.getText());

                if(!editAddressField.getText().equalsIgnoreCase(allContacts.get(selectedItem).getAddress()))
                    allContacts.get(selectedItem).setAddress(editAddressField.getText());

                XMLParser.editContact(allContacts, filePath);

                contactsTable.setValueAt(allContacts.get(selectedItem).getName(), selectedItem, 0);
                contactsTable.setValueAt(allContacts.get(selectedItem).getPhone(), selectedItem, 1);
                contactsTable.setValueAt(allContacts.get(selectedItem).getEmail(), selectedItem, 2);
                contactsTable.setValueAt(allContacts.get(selectedItem).getDOB(), selectedItem, 3);
                contactsTable.setValueAt(allContacts.get(selectedItem).getGender(), selectedItem, 4);
                contactsTable.setValueAt(allContacts.get(selectedItem).getAddress(), selectedItem, 5);

                cl.show(baseCard, "Home");
            }
        });

        editButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                cl.show(baseCard, "Edit");
                int id = contactsTable.getSelectedRow();
                editNameField.setText(allContacts.get(id).getName());
                editPhoneField.setText(allContacts.get(id).getPhone());
                editEmailField.setText(allContacts.get(id).getEmail());
                editDOBField.setText(allContacts.get(id).getDOB());
                editGenderField.setText(allContacts.get(id).getGender());
                editAddressField.setText(allContacts.get(id).getAddress());

                editContactNameStatusLabel.setText("");
                editContactPhoneStatusLabel.setText("");
                editContactEmailStatusLabel.setText("");
                editContactDOBStatusLabel.setText("");
                editContactGenderStatusLabel.setText("");
                editContactAddressStatusLabel.setText("");

                // koi bhi field change karo name overwrite hora
            }
        });

        deleteButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                selectedItem = contactsTable.getSelectedRow();
                int answer = JOptionPane.showConfirmDialog((Component)deleteButton, "Are you sure you want to delete the contact", "Delete", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.ERROR_MESSAGE);
                if(answer == 0) {
                    allContacts.remove(selectedItem);
                    XMLParser.deleteContact(allContacts, filePath);
                    defTableModel.removeRow(contactsTable.getSelectedRow());
                }
            }
        });

        searchButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.out.println("HI");
                ArrayList<Contact> searchResult = new ArrayList<>();
                String searchParam = searchField.getText();

                for(Contact contact:allContacts) {
                    if(contact.getName().contains(searchParam) || contact.getPhone().contains(searchParam) || contact.getEmail().contains(searchParam) || contact.getDOB().contains(searchParam) || contact.getGender().contains(searchParam) || contact.getAddress().contains(searchParam))
                        searchResult.add(contact);                
                }

                if(!searchResult.isEmpty()) {
                    System.out.println("Res to hai");
                    contactsTable.removeAll();
                    Iterator searchResultIterator = searchResult.iterator();
                    while(searchResultIterator.hasNext()) {
                        Contact result = (Contact)searchResultIterator.next();
                        Vector<String> contact = new Vector<>();
                        
                        System.out.println(result.getName());
                        System.out.println(result.getPhone());
                        System.out.println(result.getEmail());
                        System.out.println(result.getDOB());
                        System.out.println(result.getGender());
                        System.out.println(result.getAddress());

                        contact.add(result.getName());
                        contact.add(result.getPhone());
                        contact.add(result.getEmail());
                        contact.add(result.getDOB());
                        contact.add(result.getGender());
                        contact.add(result.getAddress());
                    }
                }
            }
        });

        backButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                cl.show(baseCard, "Home");
                // XMLParser.getContacts(filePath);
            }
        });

        backButton2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                cl.show(baseCard, "Home");
                // XMLParser.getContacts(filePath);
            }
        });
        setVisible(true);
    }
    public boolean isDirty(int id, String newName, String newPhone, String newEmail, String newDOB, String newGender, String newAddress) {
        if(!(newName.equalsIgnoreCase(allContacts.get(id).getName()) && newPhone.equalsIgnoreCase(allContacts.get(id).getPhone()) && newEmail.equalsIgnoreCase(allContacts.get(id).getEmail()) && newDOB.equalsIgnoreCase(allContacts.get(id).getDOB()) && newGender.equalsIgnoreCase(allContacts.get(id).getGender()) && newAddress.equalsIgnoreCase(allContacts.get(id).getAddress())))
            return true;
        return false;
    }
    public static void main(String[] args) {
        new ContactDemo();
    }
}
