import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class XMLParser {
    // Regex Strings
    private static String contactTagRegex = "<contact>([\\s\\S]*?)</contact>";
    private static String nameTagRegex = "<name>([\\w\\s]*?)</name>";
    private static String phoneTagRegex = "<phone_number>(\\d{10}?)</phone_number>";
    private static String emailTagRegex = "<email>([A-Za-z_]([\\.A-Za-z0-9\\+-_]+)*@([A-Za-z_]([A-Za-z0-9-])*)(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z0-9]{2,})?)</email>";
    private static String dobTagRegex = "<dob>(\\d{2}-\\d{2}-\\d{4}?)</dob>";
    private static String genderTagRegex = "<gender>([mMfF]?)</gender>";
    private static String addressTagRegex = "<address>([\\s\\S]+?)</address>";

    public static ArrayList<Contact> getContacts(String filePath) {
        ArrayList<Contact> allContacts = new ArrayList<>();
        StringBuffer fileContent = new StringBuffer(); 
        File fileName = new File(filePath);
        try {
            FileInputStream fis = new FileInputStream(fileName);
            BufferedReader br = new BufferedReader(new InputStreamReader(fis));
            String currentLine;
            while((currentLine = br.readLine()) != null) {
                fileContent.append(currentLine);
            }
            fis.close();
            br.close();            
        } catch(FileNotFoundException e) {
            System.out.println("No file found");
        } catch(IOException e) {
            System.out.println("IO Exception");
        }
        // Regex Patterns
        Pattern contactPattern = Pattern.compile(contactTagRegex);
        Pattern namePattern = Pattern.compile(nameTagRegex);
        Pattern phonePattern = Pattern.compile(phoneTagRegex);
        Pattern emailPattern = Pattern.compile(emailTagRegex);
        Pattern dobPattern = Pattern.compile(dobTagRegex);
        Pattern genderPattern = Pattern.compile(genderTagRegex);
        Pattern addressPattern = Pattern.compile(addressTagRegex);

        // Regex Matchers
        Matcher contactMatcher = contactPattern.matcher(fileContent);
        Matcher nameMatcher = namePattern.matcher(fileContent);
        Matcher phoneMatcher = phonePattern.matcher(fileContent);
        Matcher emailMatcher = emailPattern.matcher(fileContent);
        Matcher dobMatcher = dobPattern.matcher(fileContent);
        Matcher genderMatcher = genderPattern.matcher(fileContent);
        Matcher addressMatcher = addressPattern.matcher(fileContent);

        // Adding individual contacts to ArrayList
        while(contactMatcher.find()) {
            String name = "", phone = "", email = "", dob = "", gender = "", address = "";
            if(nameMatcher.find()) {
                // System.out.println("Name: " + nameMatcher.group(1));
                name = nameMatcher.group(1);
            }
            if(phoneMatcher.find()) {
                // System.out.println("Phone: " + phoneMatcher.group(1));
                phone = phoneMatcher.group(1);
            }
            if(emailMatcher.find()) {
                // System.out.println("Email: " + emailMatcher.group(1));
                email = emailMatcher.group(1);
            }
            if(dobMatcher.find()) {
                // System.out.println("DOB: " + dobMatcher.group(1));
                dob = dobMatcher.group(1);
            }
            if(genderMatcher.find()) {
                // System.out.println("Gender: " + genderMatcher.group(1));
                gender = genderMatcher.group(1);
            }
            if(addressMatcher.find()) {
                // System.out.println("Address: " + addressMatcher.group(1));
                address = addressMatcher.group(1);
            }
            Contact dummy = new Contact(name, phone, email, dob, gender, address);
            allContacts.add(dummy);
        }
        return allContacts;
    }

    public static ArrayList<Contact> addContact(Contact newContact, String filePath) {
        ArrayList<Contact> contacts = new ArrayList<>();
        File fileName = new File(filePath);
        contacts = getContacts(filePath);
        contacts.add(newContact);
        String updatedContactsListAsString = XMLParser.contactArrayListToString(contacts);
        XMLParser.rewriteFile(fileName, updatedContactsListAsString);
        return contacts;
    }

    public static void editContact(ArrayList<Contact> editedContactList, String filePath) {
        File fileName = new File(filePath);
        String updatedContactsListAsString = XMLParser.contactArrayListToString(editedContactList);
        XMLParser.rewriteFile(fileName, updatedContactsListAsString);
    }

    public static void deleteContact(ArrayList<Contact> updatedContactList, String filePath) {
        File fileName = new File(filePath);
        String updatedContactsListAsString = XMLParser.contactArrayListToString(updatedContactList);
        XMLParser.rewriteFile(fileName, updatedContactsListAsString);
    }

    // Helper functions
    public static String contactArrayListToString(ArrayList<Contact> contacts) {
        StringBuffer updatedData = new StringBuffer();
        updatedData.append("<contact_list>\n");
        for(Contact contact:contacts) {
            updatedData.append("\t<contact>\n");
            updatedData.append("\t\t<name>" + contact.getName() + "</name>\n");
            updatedData.append("\t\t<phone_number>" + contact.getPhone() + "</phone_number>\n");
            updatedData.append("\t\t<email>" + contact.getEmail() + "</email>\n");
            updatedData.append("\t\t<dob>" + contact.getDOB() + "</dob>\n");
            updatedData.append("\t\t<gender>" + contact.getGender() + "</gender>\n");
            updatedData.append("\t\t<address>" + contact.getAddress() + "</address>");
            updatedData.append("\n\t</contact>\n");
        }
        updatedData.append("</contact_list>\n");
        String updatedString = updatedData.toString();
        return updatedString;
    }

    public static void rewriteFile(File fileName, String updatedString) {
        try{
            FileOutputStream fos = new FileOutputStream(fileName, false);
            String nullString = "";
            fos.write(nullString.getBytes());
            fos.write(updatedString.getBytes());
            fos.close();
        } catch(FileNotFoundException e) {
            System.out.println("No file found");
        } catch(IOException e) {
            System.out.println("IO Exception occured");
        }
    }
}
