This is a Contact Management software built using Java.
It uses an XML file to store the data.
A custom XML parser has been developed to read and write to the same XML file, without using any frameworks.
It also implements concepts such as ArrayLists, Anonymous Inner Classes, Java Swing etc.